import React,{Component}  from 'react';
// import { Route,Redirect } from 'react-router-dom';



class Form extends Component {


  constructor(props)
   {
     super(props);
     this.state = { checked: false,  payment_amount: 0, amount: 10000};
     this.handleChange = this.handleChange.bind(this);
     this.paymentHandler = this.paymentHandler.bind(this);
     // this.changeAmount = this.changeAmount.bind(this);
   }


      state = {

            name: '',
            standard: '',
            school: '',
            gender: '',
            email: '',
          };

    handleName = (e) => {
        this.setState({
            name: e.target.value
        })
    }

    handleStandard = (e) => {
        this.setState({
            standard: e.target.value
        });
    }


    handleSchool = (e) => {
        this.setState({
            school: e.target.value
        })
    }


    handleGender = (e) => {
        this.setState({
            gender: e.target.value
        })
    }


    handleEmail = (e) => {
        this.setState({
            email: e.target.value
        })
    }

    handleSubmit = (e) => {
		e.preventDefault();
    this.props.addUser(this.state.name, this.state.standard, this.state.school, this.state.gender, this.state.email);
    this.setState({name: '',standard: '',school: '',gender: '',email: ''})

     let options =
      {
        "key": "rzp_test_3OHEkrM9aPMz5u",
        "amount": this.state.amount, // 2000 paise = INR 20, amount in paisa
        "name": "ProBano",
        "description": "Cpr Test",
        "image": "https://cdn.razorpay.com/logos/AZ1iYZZJwy1kdz_medium.png",
        "handler":function (response){
               window.location = "http://localhost:3000/Result";
               // <Redirect to="/Cpr_1/" />
        },
        "prefill":
        {
          "name": " ",
          "email": ""
        },
        "notes":
        {
          "address": "Hello World"
        },
        "theme":
        {
          "color": "#528FF0"
        }
      };
      let rzp = new window.Razorpay(options);
      rzp.open();


	}


////////////////// RazorPay Integration ///////////////////////


componentDidMount ()
   {
     const script = document.createElement("script");
     script.src = "https://checkout.razorpay.com/v1/checkout.js";
     script.async = true;
     document.body.appendChild(script);
   }


   handleChange()
   {
     this.setState(
       {
         checked: !this.state.checked
       })
    }

    /*changeAmount(e)
    {
      this.setState({amount: e.target.value})
    }*/
    paymentHandler(e)
    {
      e.preventDefault();
     }




/////////////////////// RazorPay Ending /////////////////////////////////////////





    render() {
        const { name, standard, school, gender, email }=this.state
        return(





            <div >
            <form onSubmit={this.handleSubmit} >
               <div >
                   <label className="contents"> Name </label>
                   <br />
                   <input className="inputs"
                   type="text"
                   placeholder="Enter your name"
                   value={name}
                   onChange={this.handleName}
                    />
               </div>
               <br />
               <br />
               <div>
                   <label className="contents"> Class </label>
                   <br />
                   <input className="inputs"
                   type="text"
                   placeholder="Enter your Standard"
                   value={standard}
                   onChange={this.handleStandard}
                   />
                </div>
                <br />
                <br />
               <div>
                   <label className="contents" > School </label>
                   <br />
                   <input className="inputs"
                   type="text"
                   placeholder="Enter your School"
                   value={school}
                   onChange={this.handleSchool}
                    />
               </div>
               <br />
               <br />
               <div>
                   <label className="contents" className=""> E-mail </label>
                   <br />
                   <input className="inputs"
                   type="text"
                   placeholder="Enter your E-mail"
                   value={email}
                   onChange={this.handleEmail}
                    />
               </div>
               <br />
               <br />
               <div>
                   <label className="contents" > Gender </label>
                   <br />
                   <input type="radio" onChange={this.handleGender} name={gender} value="male"  /> Male
                   <br />
                   <input type="radio" onChange={this.handleGender} name={gender} value="female" /> Female<br />
                   <br />
               </div>
               <br />
                <button className="button-form"
                 type="submit">Proceed to Pay</button>
            </form>
          </div>

        );
    }
}

export default Form;
