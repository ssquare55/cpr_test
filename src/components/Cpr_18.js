import React,{Component} from 'react';
import { BrowserRouter,Route } from 'react-router-dom';

class Cpr_18 extends Component {

  state = {
    visible1: true,
    visible2: true,
    visible3: true,
    visible4: true,
    counter: 0,
    priority1: 0,
    priority2: 0,
    priority3: 0,
    priority4: 0,

  };


  handlClick1 = (e) => {
    if(this.state.visible1)
    {
      this.setState({
        counter: this.state.counter += 1,
        visible1: !this.state.visible1,
        priority1: this.state.counter
      });
    }
       
       
  }

  handlClick2 = (e) => {
    if(this.state.visible2)
    {
      this.setState({
        counter: this.state.counter += 1,
        visible2: !this.state.visible2,
        priority2: this.state.counter
      });
    }
       
  }

  handlClick3 = (e) => {
    if(this.state.visible3)
    {
      this.setState({
        counter: this.state.counter += 1,
        visible3: !this.state.visible3,
        priority3: this.state.counter
      });
    }
       
  }

  handlClick4 = (e) => {

    if(this.state.visible4)
    {
      this.setState({
        counter: this.state.counter += 1,
        visible4: !this.state.visible4,
        priority4: this.state.counter
      });
    }
       

  }

  render() {
  
    if(this.state.visible1===false && this.state.visible2===false && this.state.visible3===false && this.state.visible4===false)
    {
      this.props.handleResult(this.state.priority1, this.state.priority2, this.state.priority3, this.state.priority4);
      // window.location = "http://localhost:3000/Cpr_19";
    }
      return (
              <div className="quiz-container">

                    <h2>Rank these careers based on your preference.</h2>
                    <table>
                    <tbody>
                    <tr>
                        <td>
                        <input className="input1" type="image" alt="image" title=" " src={this.state.visible1 ? "doctor.png" : this.state.priority1.toString() +".png" }  onClick={this.handlClick1} />
                        <div>
                        <h3>Homeopathy Doctor</h3>
                        </div>
                        </td>
                        <td>
                        <input className="input1" type="image" alt="image" title=" " src={this.state.visible2 ? "engineer.png" : this.state.priority2.toString() +".png" }  onClick={this.handlClick2} />
                        <div>
                        <h3>Mechanical Engineer</h3>
                        </div>
                        </td>
                  </tr>
                  <tr>
                        <td>
                        <input className="input1" type="image" alt="image" title=" " src={this.state.visible3 ? "designer.png" : this.state.priority3.toString() +".png" }  onClick={this.handlClick3} />
                        <div>
                        <h3>Automobile Designer</h3>
                        </div>
                        </td>
                        <td>
                        <input className="input1" type="image" alt="image" title=" " src={this.state.visible4 ? "bachelor of science.png" : this.state.priority4.toString() +".png" }  onClick={this.handlClick4} />
                        <div>
                        <h3>Lab Technician</h3>
                        </div>
                        </td>
                        </tr>
                        </tbody>
                        </table>
            </div>
    );
  }

}
export default Cpr_18;
