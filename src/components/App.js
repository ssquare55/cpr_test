import React,{Component} from 'react';
import {
  BrowserRouter,
  Route
} from 'react-router-dom';

//App Components
import Form_Data from './Form_Data';
import Result from './Result';
import Cpr_1 from './Cpr_1';
import Cpr_2 from './Cpr_2';
import Cpr_3 from './Cpr_3';
import Cpr_4 from './Cpr_4';
import Cpr_5 from './Cpr_5';
import Cpr_6 from './Cpr_6';
import Cpr_7 from './Cpr_7';
import Cpr_8 from './Cpr_8';
import Cpr_9 from './Cpr_9';
import Cpr_10 from './Cpr_10';
import Cpr_11 from './Cpr_11';
import Cpr_12 from './Cpr_12';
import Cpr_13 from './Cpr_13';
import Cpr_14 from './Cpr_14';
import Cpr_15 from './Cpr_15';
import Cpr_16 from './Cpr_16';
import Cpr_17 from './Cpr_17';
import Cpr_18 from './Cpr_18';
import Cpr_19 from './Cpr_19';
import Cpr_20 from './Cpr_20';
import Cpr_21 from './Cpr_21';
import Cpr_22 from './Cpr_22';
import Cpr_23 from './Cpr_23';
import Cpr_24 from './Cpr_24';
import Cpr_25 from './Cpr_25';
import Cpr_26 from './Cpr_26';
import Cpr_27 from './Cpr_27';
import Cpr_28 from './Cpr_28';
import Cpr_29 from './Cpr_29';
import Cpr_30 from './Cpr_30';
import Cpr_31 from './Cpr_31';
import Cpr_32 from './Cpr_32';
import Cpr_33 from './Cpr_33';
import Cpr_34 from './Cpr_34';
import Cpr_35 from './Cpr_35';


class App extends Component{
render() {
return (
  <BrowserRouter>
  <div className="form-div">
    <Route exact path="/" component={Form_Data} />
  <Route path="/Result" component={Result} />
    <Route path="/Cpr_1" component={Cpr_1} />
    <Route path="/Cpr_2" component={Cpr_2} />
    <Route path="/Cpr_3" component={Cpr_3} />
    <Route path="/Cpr_4" component={Cpr_4} />
    <Route path="/Cpr_5" component={Cpr_5} />
    <Route path="/Cpr_6" component={Cpr_6} />
    <Route path="/Cpr_7" component={Cpr_7} />
    <Route path="/Cpr_8" component={Cpr_8} />
    <Route path="/Cpr_9" component={Cpr_9} />
    <Route path="/Cpr_10" component={Cpr_10} />
    <Route path="/Cpr_11" component={Cpr_11} />
    <Route path="/Cpr_12" component={Cpr_12} />
    <Route path="/Cpr_13" component={Cpr_13} />
    <Route path="/Cpr_14" component={Cpr_14} />
    <Route path="/Cpr_15" component={Cpr_15} />
    <Route path="/Cpr_16" component={Cpr_16} />
    <Route path="/Cpr_17" component={Cpr_17} />
    <Route path="/Cpr_18" component={Cpr_18} />
    <Route path="/Cpr_19" component={Cpr_19} />
    <Route path="/Cpr_20" component={Cpr_20} />
    <Route path="/Cpr_21" component={Cpr_21} />
    <Route path="/Cpr_22" component={Cpr_22} />
    <Route path="/Cpr_23" component={Cpr_23} />
    <Route path="/Cpr_24" component={Cpr_24} />
    <Route path="/Cpr_25" component={Cpr_25} />
    <Route path="/Cpr_26" component={Cpr_26} />
    <Route path="/Cpr_27" component={Cpr_27} />
    <Route path="/Cpr_28" component={Cpr_28} />
    <Route path="/Cpr_29" component={Cpr_29} />
    <Route path="/Cpr_30" component={Cpr_30} />
    <Route path="/Cpr_31" component={Cpr_31} />
    <Route path="/Cpr_32" component={Cpr_32} />
    <Route path="/Cpr_33" component={Cpr_33} />
    <Route path="/Cpr_34" component={Cpr_34} />
    <Route path="/Cpr_35" component={Cpr_35} />
  </div>
  </BrowserRouter>


 );
}

}

export default App;
