import React,{Component} from 'react';
import {Route,Router} from 'react-router-dom';
import Cpr_2 from './Cpr_2';
import Cpr_1 from './Cpr_1';
import Cpr_3 from './Cpr_3';
import Cpr_4 from './Cpr_4';
import Cpr_5 from './Cpr_5';
import Cpr_6 from './Cpr_6';
import Cpr_7 from './Cpr_7';
import Cpr_8 from './Cpr_8';
import Cpr_9 from './Cpr_9';
import Cpr_12 from './Cpr_12';
import Cpr_10 from './Cpr_10';
import Cpr_11 from './Cpr_11';
import Cpr_13 from './Cpr_13';
import Cpr_14 from './Cpr_14';
import Cpr_15 from './Cpr_15';
import Cpr_16 from './Cpr_16';
import Cpr_17 from './Cpr_17';
import Cpr_18 from './Cpr_18';
import Cpr_19 from './Cpr_19';
import Cpr_20 from './Cpr_20';
import Cpr_21 from './Cpr_21';
import Cpr_22 from './Cpr_22';
import Cpr_23 from './Cpr_23';
import Cpr_24 from './Cpr_24';
import Cpr_25 from './Cpr_25';
import Cpr_26 from './Cpr_26';
import Cpr_27 from './Cpr_27';
import Cpr_28 from './Cpr_28';
import Cpr_29 from './Cpr_29';
import Cpr_30 from './Cpr_30';
import Cpr_31 from './Cpr_31';
import Cpr_32 from './Cpr_32';
import Cpr_33 from './Cpr_33';
import Cpr_34 from './Cpr_34';
import Cpr_35 from './Cpr_35';

var engineering=[];
var medical_science=[];
var design=[];
var education=[];
var law=[];
var civil_services=[];
var social_work=[];
var tourism=[];
var mass_media=[];
var commercial=[];
var pure_science=[];
var humanities=[];
var defence=[];
var miscellaneous=[];

var sum_ofengineering=0;
var sum_ofmedical_science=0;
var sum_ofdesign=0;
var sum_ofeducation=0;
var sum_oflaw=0
var sum_ofcivil_services=0;
var sum_ofsocial_work=0;
var sum_oftourism=0;
var sum_ofmass_media=0;
var sum_ofcommercial=0;
var sum_ofpure_science=0;
var sum_ofhumanities=0;
var sum_ofdefence=0;
var sum_ofmiscellaneous=0;

var finalArray=[];



class Result extends Component {

  handleCpr_1 = (option1, option2, option3, option4) => {
    engineering.push(option1);
    medical_science.push(option2);
    design.push(option3);
    education.push(option4);

    // console.log();
    // alert("hold");
  }

  handleCpr_2 = (option1, option2, option3, option4) => {
    law.push(option1);
    civil_services.push(option2);
    social_work.push(option3);
    tourism.push(option4);
    // alert("hold");
  }

  handleCpr_3 = (option1, option2, option3, option4) => {
    mass_media.push(option1);
    commercial.push(option2);
    pure_science.push(option3);
    humanities.push(option4);
    // alert("hold");
  }

  handleCpr_4 = (option1, option2, option3, option4) => {
    defence.push(option1);
    miscellaneous.push(option2);
    engineering.push(option3);
    medical_science.push(option4);
    // alert("hold");
  }

  handleCpr_5 = (option1, option2, option3, option4) => {
    design.push(option1);
    education.push(option2);
    law.push(option3);
    civil_services.push(option4);
    // alert("hold");
  }

  handleCpr_6 = (option1, option2, option3, option4) => {
    social_work.push(option1);
    tourism.push(option2);
    mass_media.push(option3);
    commercial.push(option4);
    // alert("hold");
  }

  handleCpr_7 = (option1, option2, option3, option4) => {
    pure_science.push(option1);
    humanities.push(option2);
    defence.push(option3);
    miscellaneous.push(option4);
    // alert("hold");
  }

  handleCpr_8 = (option1, option2, option3, option4) => {
    engineering.push(option1);
    medical_science.push(option2);
    design.push(option3);
    education.push(option4);
    // alert("hold");
  }


  handleCpr_9 = (option1, option2, option3, option4) => {
    mass_media.push(option1);
    commercial.push(option2);
    pure_science.push(option3);
    humanities.push(option4);
    // alert("hold");
  }

  handleCpr_10 = (option1, option2, option3, option4) => {
    law.push(option1);
    civil_services.push(option2);
    social_work.push(option3);
    tourism.push(option4);
    // alert("hold");
  }

  handleCpr_11 = (option1, option2, option3, option4) => {
    defence.push(option1);
    miscellaneous.push(option2);
    engineering.push(option3);
    medical_science.push(option4);
    // alert("hold");
  }

  handleCpr_12 = (option1, option2, option3, option4) => {
    design.push(option1);
    education.push(option2);
    law.push(option3);
    civil_services.push(option4);
    // alert("hold");
  }

  handleCpr_13 = (option1, option2, option3, option4) => {
    social_work.push(option1);
    tourism.push(option2);
    mass_media.push(option3);
    commercial.push(option4);
    // alert("hold");
  }

  handleCpr_14 = (option1, option2, option3, option4) => {
    pure_science.push(option1);
    humanities.push(option2);
    defence.push(option3);
    miscellaneous.push(option4);
    // alert("hold");
  }

  handleCpr_15 = (option1, option2, option3, option4) => {
    miscellaneous.push(option1);
    defence.push(option2);
    humanities.push(option3);
    pure_science.push(option4);
    // alert("hold");
  }

  handleCpr_16 = (option1, option2, option3, option4) => {
    commercial.push(option1);
    mass_media.push(option2);
    tourism.push(option3);
    social_work.push(option4);
    // alert("hold");
  }

  handleCpr_17 = (option1, option2, option3, option4) => {
    civil_services.push(option1);
    law.push(option2);
    education.push(option3);
    design.push(option4);
    // alert("hold");
  }

  handleCpr_18 = (option1, option2, option3, option4) => {
    medical_science.push(option1);
    engineering.push(option2);
    design.push(option3);
    education.push(option4);
    // alert("hold");
  }

  handleCpr_19 = (option1, option2, option3, option4) => {
    engineering.push(option1);
    law.push(option2);
    medical_science.push(option3);
    civil_services.push(option4);
    // alert("hold");
  }

  handleCpr_20 = (option1, option2, option3, option4) => {
    social_work.push(option1);
    tourism.push(option2);
    mass_media.push(option3);
    commercial.push(option4);
    // alert("hold");
  }

  handleCpr_21 = (option1, option2, option3, option4) => {
    pure_science.push(option1);
    humanities.push(option2);
    defence.push(option3);
    miscellaneous.push(option4);
    // alert("hold");
  }

  handleCpr_22 = (option1, option2, option3, option4) => {
    engineering.push(option1);
    medical_science.push(option2);
    design.push(option3);
    education.push(option4);
    // alert("hold");
  }

  handleCpr_23 = (option1, option2, option3, option4) => {
    law.push(option1);
    civil_services.push(option2);
    social_work.push(option3);
    tourism.push(option4);
    // alert("hold");
  }

  handleCpr_24 = (option1, option2, option3, option4) => {
    mass_media.push(option1);
    defence.push(option2);
    miscellaneous.push(option3);
    engineering.push(option4);
    // alert("hold");
  }

  handleCpr_25 = (option1, option2, option3, option4) => {
    commercial.push(option1);
    pure_science.push(option2);
    humanities.push(option3);
    law.push(option4);
    // alert("hold");
  }

  handleCpr_26 = (option1, option2, option3, option4) => {
    medical_science.push(option1);
    social_work.push(option2);
    mass_media.push(option3);
    commercial.push(option4);
    // alert("hold");
  }

  handleCpr_27 = (option1, option2, option3, option4) => {
    design.push(option1);
    miscellaneous.push(option2);
    defence.push(option3);
    humanities.push(option4);
    // alert("hold");
  }

  handleCpr_28 = (option1, option2, option3, option4) => {
    education.push(option1);
    civil_services.push(option2);
    tourism.push(option3);
    pure_science.push(option4);
    // alert("hold");
  }

  handleCpr_29 = (option1, option2, option3, option4) => {
    engineering.push(option1);
    tourism.push(option2);
    civil_services.push(option3);
    social_work.push(option4);
    // alert("hold");
  }

  handleCpr_30 = (option1, option2, option3, option4) => {
    mass_media.push(option1);
    commercial.push(option2);
    design.push(option3);
    medical_science.push(option4);
    // alert("hold");
  }

  handleCpr_31 = (option1, option2, option3, option4) => {
    education.push(option1);
    law.push(option2);
    pure_science.push(option3);
    humanities.push(option4);
    // alert("hold");
  }

  handleCpr_32 = (option1, option2, option3, option4) => {
    miscellaneous.push(option1);
    mass_media.push(option2);
    civil_services.push(option3);
    education.push(option4);
    // alert("hold");
  }

  handleCpr_33 = (option1, option2, option3, option4) => {
    defence.push(option1);
    miscellaneous.push(option2);
    law.push(option3);
    design.push(option4);
    // alert("hold");
  }

  handleCpr_34 = (option1, option2, option3, option4) => {
    engineering.push(option1);
    social_work.push(option2);
    tourism.push(option3);
    commercial.push(option4);
    // alert("hold");
  }

  handleCpr_35 = (option1, option2, option3, option4) => {
    medical_science.push(option1);
    pure_science.push(option2);
    humanities.push(option3);
    defence.push(option4);
    // alert("hold");
    for(var i in engineering) { sum_ofengineering  += engineering[i];  }
    finalArray.push(sum_ofengineering);

    for(var i in medical_science) { sum_ofmedical_science  += medical_science[i];  }
    finalArray.push(sum_ofmedical_science);

    for(var i in design) { sum_ofdesign  += design[i];  }
    finalArray.push(sum_ofdesign);

    for(var i in education) { sum_ofeducation  += education[i];  }
    finalArray.push(sum_ofeducation);

    for(var i in law) { sum_oflaw  += law[i];  }
    finalArray.push(sum_oflaw);

    for(var i in civil_services) { sum_ofcivil_services  += civil_services[i];  }
    finalArray.push(sum_ofcivil_services);

    for(var i in social_work) { sum_ofsocial_work  += social_work[i];  }
    finalArray.push(sum_ofsocial_work);

    for(var i in tourism) { sum_oftourism  += tourism[i];  }
    finalArray.push(sum_oftourism);

    for(var i in mass_media) { sum_ofmass_media  += mass_media[i];  }
    finalArray.push(sum_ofmass_media);

    for(var i in commercial) {sum_ofcommercial += commercial[i]; }
    finalArray.push(sum_ofcommercial);

    for(var i in pure_science) {sum_ofpure_science += pure_science[i]; }
    finalArray.push(sum_ofpure_science);

    for(var i in humanities) {sum_ofhumanities += humanities[i]; }
    finalArray.push(sum_ofhumanities);

    for(var i in defence) {sum_ofdefence += defence[i]; }
    finalArray.push(sum_ofdefence);

    for(var i in miscellaneous) {sum_ofmiscellaneous += miscellaneous[i]; }
    finalArray.push(sum_ofmiscellaneous);



    // console.log(sum_ofengineering);
    // console.log(sum_ofmedical_science);
    // console.log(sum_ofdesign);
    // console.log(sum_ofeducation);
    // console.log(sum_oflaw);
    // console.log(sum_ofcivil_services);
    // console.log(sum_ofsocial_work);
    // console.log(sum_oftourism);
    // console.log(sum_ofmass_media);
    // console.log(sum_ofcommercial);
    // console.log(sum_ofpure_science);
    // console.log(sum_ofhumanities);
    // console.log(sum_ofdefence);
    // console.log(sum_ofmiscellaneous);
    alert("yes");


  }




  render() {
    return(
      <div>
      <Cpr_1 handleResult={this.handleCpr_1} />
      <Cpr_2 handleResult={this.handleCpr_2} />
      <Cpr_3 handleResult={this.handleCpr_3} />
      <Cpr_4 handleResult={this.handleCpr_4} />
      <Cpr_5 handleResult={this.handleCpr_5} />
      <Cpr_6 handleResult={this.handleCpr_6} />
      <Cpr_7 handleResult={this.handleCpr_7} />
      <Cpr_8 handleResult={this.handleCpr_8} />
      <Cpr_9 handleResult={this.handleCpr_9} />
      <Cpr_10 handleResult={this.handleCpr_10} />
      <Cpr_11 handleResult={this.handleCpr_11} />
      <Cpr_12 handleResult={this.handleCpr_12} />
      <Cpr_13 handleResult={this.handleCpr_13} />
      <Cpr_14 handleResult={this.handleCpr_14} />
      <Cpr_15 handleResult={this.handleCpr_15} />
      <Cpr_16 handleResult={this.handleCpr_16} />
      <Cpr_17 handleResult={this.handleCpr_17} />
      <Cpr_18 handleResult={this.handleCpr_18} />
      <Cpr_19 handleResult={this.handleCpr_19} />
      <Cpr_20 handleResult={this.handleCpr_20} />
      <Cpr_21 handleResult={this.handleCpr_21} />
      <Cpr_22 handleResult={this.handleCpr_22} />
      <Cpr_23 handleResult={this.handleCpr_23} />
      <Cpr_24 handleResult={this.handleCpr_24} />
      <Cpr_25 handleResult={this.handleCpr_25} />
      <Cpr_26 handleResult={this.handleCpr_26} />
      <Cpr_27 handleResult={this.handleCpr_27} />
      <Cpr_28 handleResult={this.handleCpr_28} />
      <Cpr_29 handleResult={this.handleCpr_29} />
      <Cpr_30 handleResult={this.handleCpr_30} />
      <Cpr_31 handleResult={this.handleCpr_31} />
      <Cpr_32 handleResult={this.handleCpr_32} />
      <Cpr_33 handleResult={this.handleCpr_33} />
      <Cpr_34 handleResult={this.handleCpr_34} />
      <Cpr_35 handleResult={this.handleCpr_35}  />
      </div>
    );
  }


}
export default Result;
